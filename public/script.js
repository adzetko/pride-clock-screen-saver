function showTime() {
	var date = new Date();
	var h = date.getHours(); // 0 - 23
	var m = date.getMinutes(); // 0 - 59
	var s = date.getSeconds(); // 0 - 59

	h = h < 10 ? "0" + h : h;
	m = m < 10 ? "0" + m : m;
	s = s < 10 ? "0" + s : s;

	document.getElementById("hour").innerText = h;
	document.getElementById("hour").textContent = h;

	document.getElementById("min").innerText = m;
	document.getElementById("min").textContent = m;

	document.getElementById("sec").innerText = s;
	document.getElementById("sec").textContent = s;

	setTimeout(showTime, 1000);
}

showTime();